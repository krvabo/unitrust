<?
namespace model;

/**
 * currency :: 
 *
 * @author Krijn
 * @version 1
 */
class Currency
{

   /** 
    * Name of the currency
    * 
    * @var string
    */
   protected $shortName;
   
   /**
    * constructor of currency, give me a name why dontcha
    * 
    * @param string $name
    */
   public function __construct($name) {
      $this->shortName = $name;
   }
   
   /**
    * get name of currency
    * 
    * @return string
    */
   public function getShortName()
   {
      return $this->shortName;
   }

   /**
    * Set name of currency
    * 
    * @param string $shortName
    */
   public function setShortName($shortName)
   {
      $this->shortName = $shortName;
   }


}
