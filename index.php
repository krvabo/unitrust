<?php
session_start();

require_once __DIR__ . '/vendor/autoload.php';

// normally you'd do routing here
// this is just to get everything working from one page


$action = empty($_GET['action']) ? '' : $_GET['action'];

// check if user is logged in
if (!\service\Auth::isAuth()) {
   $action = 'auth';
}

if (empty($action)) {
   
   include 'view/index.html';
   
} else {
   
   // 'routing'
   switch($action) {

      case 'auth':
         $obj = new control\Auth();
         $obj->auth();
         break;
      case 'logout':
         $obj = new control\Auth();
         $obj->logout();
         break;
      case 'currencies':
         $obj = new control\Convert();
         $obj->getCurrencies();
         break;
      default:
         $obj = new control\Convert();
         $obj->main();
   }
}