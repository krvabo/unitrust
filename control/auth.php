<?php
namespace control;

/**
 * Controller for the user to log in
 */
class Auth
{
   
   /**
    * authenticate user
    */
   public function auth()
   {
      
      // if user is already logged in
      if (\service\Auth::isAuth()) {
         header('Location: index.php');
         exit;
      }
      
      // check values
      if (empty($_POST) || empty($_POST['user']) || empty($_POST['password'])) {
         include_once('view/login.html');
         exit;
      }
      
      // check user credentials and log them in if correct
      // @todo: Actually implement something useful
      if ($_POST['user'] == 'user' && $_POST['password'] == 'bla') {
         \service\Auth::auth();
      }
      
      header('Location: index.php');
      exit;
   }
   
   public function logout()
   {
      \service\Auth::logout();
      
      header('Location: index.php');
      exit;
   }
}