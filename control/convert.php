<?php

namespace control;


/**
 * Basic handling of application requests
 */
class Convert
{
   /**
    * controller for the conversion page
    * 
    * Returns json
    */
   public function main()
   {
      
      if (!\service\Auth::isAuth()) {
         header('Location: index.php?action=auth');
         exit;
      }
      
      // if the request is invalid we'll return an empty json string
      $invalidRequestResponse = '[]';

      if (empty($_GET) || empty($_GET['value']) || empty($_GET['from']) || empty($_GET['to'])) {
         die($invalidRequestResponse);
      }

      $service = new \service\Convert();

      // check if we have valid currencies
      if (strlen($_GET['from']) != strlen($_GET['to']) && strlen($_GET['from']) != 3) {
         die($invalidRequestResponse);
      }
      

      $currencyFrom = new \model\Currency($_GET['from']);
      $currencyTo = new \model\Currency($_GET['to']);

      // the actual converting
      try {
         
         // update valid currencies
         $service->getCurrencies();

         if (!$service->isValidCurrency($_GET['from']) || !$service->isValidCurrency($_GET['to'])) {
            die($invalidRequestResponse);
         }

         $value = $service->convert($currencyFrom, $currencyTo, $_GET['value']);

         echo json_encode(['value' => $value]);

      } catch(\Exception $e) {

         // @todo trigger other http code
         echo json_encode(['error' => $e->getMessage()]);
      }
   }
   
   /**
    * return list of supported currencies
    */
   public function getCurrencies()
   {
      // check if authenticated
      if (!\service\Auth::isAuth()) {
         header('Location: index.php?action=auth');
         exit;
      }
      
      $service = new \service\Convert();

      // get currencies from service
      try {
         $currencies = $service->getCurrencies();

         echo json_encode(['currencies' => $currencies]);

      } catch(\Exception $e) {

         // @todo trigger other http code
         echo json_encode(['currencies' => [] ]);
      }
      
   }
}
