**unitrust test**


## Uitleg

Er is gewerkt in iteraties, die zijn met **tags** opgeleverd. Hierbij is uitgegaan van MVP, minimal viable product.

**tag 1**: Werkende conversie, zonder authenticatie, met 'support' voor meerdere converters, en opzetten lokale omgeving (composer, git, bitbucket, e.d.)
**tag 2**: Basic authenticatie

Omdat de opdracht kan verworden in iets dat veel te groot is om nog als test aangemerkt te worden heb ik de test getimeboxed op 4.5 uur. Daarbij is het gebruik van iteraties erg prettig.

De test vraagt om authenticatie, maar dit is iets waar de rest van je site afhankelijk van zou zijn (en waar de functionaliteit afhankelijk van zou zijn), dus deze is zeer minimaal geimplementeerd. 

Iteratie 3 is niet af. De weergave van de aangeboden currencies op de frontend is kapot. Vanwege paniek in huiselijke sferen (de kraan is afgebroken om half 11 's avonds) kan ik op dit moment niet meer tijd besteden aan de opdracht. Daarom is deze functionaliteit geoutcomment.

---

## Fixer.io

Sinds enige tijd werkt Fixer.io met een nieuw systeem. Hoewel het nogsteeds gratis is kan de baseCurrency niets anders zijn dan EUR tenzij je een betaald abonnement neemt.

Deze api valt dus af voor de opdracht omdat er tussen verschillende currencies gewerkt moet worden. Het is uiteraard mogelijk hier omheen te werken, maar dit kan leiden tot afrondingsfouten.


---

## Tijd

**iteratie 1** Basic versie met Kowabunga, zonder authenticatie, met omgeving instellen: 3 uur
**iteratie 2** Basic authenticatie: 30-40 minuten
**iteratie 3** Supporten van alle aangeboden currencies **incomplete**: een gebroken 45 minuten.

---

### Dev

Ik heb de vendor-map meegecommit zodat er geen losse composer install gedraaid hoeft te worden om te werken. Normaal zou de vendor map niet in de source control komen.

Mijn lokale omgeving is al lange tijd niet meer gebruikt, dus is ook nog niet geupdate aan mijn 'nieuwe' manier van programmeren. Het kan dus zo zijn dat ik wat code over het hoofd heb gezien die niet voldoet aan de PSR-standaard, waar deze dit wel zou moeten zijn (mbt accolades e.d.).

De convertor services hangen onder een interface zodat nieuwe services relatief makkelijk toe te voegen zijn.

Voor de view heb ik geen gebruik gemaakt van een template engine, hoewel ik dat normaal wel zou doen. Dit vanwege het feit dat er nauwelijks GUI bij zit en dit wel relatief veel tijd zou kosten om toe te voegen.

Normaalgesproken zit je bij het ontwikkelen van functionaliteit natuurlijk al in een soort framework, maar dat ontbreekt bij deze test. Opzetten hiervan kost natuurlijk tijd, en ik heb daarom gekozen om het zo minimaal mogelijk te maken.
Daarbij heb ik wel een standaard MVC-verdeling aangehouden, met een losse folder 'service' (met symfony had ik er een bundle van gemaakt). Ik maak gebruik van composer's autoloader en een (zeer) simpele index.php welke nu hardcoded routing heeft. Normaal zou je daar ook het framework voor aanhouden, of betere routing gebruiken. Nogmaals; kleine scope en opleveren in Minimal Viable Product.