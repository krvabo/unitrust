$(function() {
   
   $('#submit').click(function() {
      
      var from = $('#currencyFrom').val();
      var to = $('#currencyTo').val();
      var value = $('#amount').val();
      $.get('index.php?action=convert&from='+from+'&to='+to+'&value='+value, function(data) {
         var result = JSON.parse(data);
         
         // show the result text
         $('#orgValue').text(value);
         $('#orgCurrency').text(from);
         $('#newValue').text(parseFloat(result.value).toFixed(2));
         $('#newCurrency').text(to);
         $('#result').removeClass('hidden');
      });
   });

   $('#switch').click(function() {
      var orgFrom = $('#currencyFrom').val();
      var orgTo = $('#currencyTo').val();
      
      $('#currencyFrom option[value='+orgTo+']').prop('selected', 'selected');
      $('#currencyTo option[value='+orgFrom+']').prop('selected', 'selected');
   })
   
   // update currencies on page load 
   
    // updateCurrencies();
    // uncommented due to bug that can't be fixed in time constraint
});

// get currencies from webservice
function updateCurrencies() {
   $.get('index.php?action=currencies', function(data) {
      var result = JSON.parse(data);
      
      // empty selects
      $('#currencyFrom, #currencyTo').find('option').remove();

      $.each(result, function(i, item) {

         $('#currencyFrom').append($('<option>'), {value: item.value, text: item.value});
      })
   })
}