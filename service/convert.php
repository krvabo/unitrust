<?php
namespace service;

use model\Currency;

/**
 * Convert currency into a different currency
 *
 * @author Krijn
 * @version 1
 */
class Convert
{
   
   /**
    * A list of accepted three char currencies (EUR, USD, ..)
    *
    * @var array
    */
   protected $currencies;
   
   /**
    * The converter we'll be using
    *
    * @var \model\currency\currencyInterface $converter
    */
   protected $converter;
   
   
   public function __construct() {
      
      // initial version will use kowabunga
      // We can decouple this later by giving a parameter
      $this->converter = new \service\currency\Kowabunga();
      
      $this->currencies = $this->converter->getSupportedCurrencies();
   }
   
   /**
    * Convert the value and return the result
    * 
    * @param \model\Currency $currencyFrom
    * @param \model\Currency $currencyTo
    * @param float $value
    * @return boolean
    */
   public function convert(Currency $currencyFrom, Currency $currencyTo, $value)
   {
      if (!$this->isValidValue($value)) {
         return false;
      }
      
      return $this->converter->convert($currencyFrom, $currencyTo, (float)$value);
   }
   
   
   /**
    * Returns if the given three letter currency code is supported. Can change between converters
    * 
    * @param type $name
    * @return type
    */
   public function isValidCurrency($name)
   {
      return in_array(strtoupper($name), $this->currencies);
   }
   
   /**
    * Check if the given value is a number of some sort
    * 
    * @param mixed $value
    * @return boolean
    */
   protected function isValidValue($value)
   {
      if (!is_numeric($value)) {
         return false;
      }
      
      // when casting the value should remain the same.
      if ((float)$value != $value && (int)$value != $value) {
         return false;
      }
      
      return true;
   }

   /**
    * Get list of supported currencies
    * 
    * @return array
    */
   public function getCurrencies()
   {
      return $this->currencies;
   }


}
