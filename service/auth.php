<?php

namespace service;

class Auth
{
   /**
    * is the user logged in?
    * 
    * @return boolean
    */
   static function isAuth() {
      return isset($_SESSION['auth']) && $_SESSION['auth'];
   }
   
   /**
    * set user as logged in
    */
   static function auth() {
      $_SESSION['auth'] = true;
   }
   
   /**
    * log user out
    */
   static function logout() {
      $_SESSION['auth'] = null;
   }
}