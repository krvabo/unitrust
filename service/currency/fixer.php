<?php
namespace service\currency;


/**
 * Fixer.io api
 *
 * @author Krijn
 * @version 1
 */
class Fixer implements \model\currency\currencyInterface
{

   /**
    * @deprecated
    * Convert currency using fixer.io
    * 
    * /!\ Not supported
    * 
    * @param \model\Currency $currencyFrom
    * @param \model\Currency $currencyTo
    * @param float $valueFrom
    * @throws \Exception
    */
   public function convert(\model\Currency $currencyFrom, \model\Currency $currencyTo, $valueFrom)
   {
      throw new \Exception('No longer supported', 100);
   }

   /**
    * Get list of supported currencies fixer.io accepts
    * 
    * @return array
    */
   public function getSupportedCurrencies() {
      return [];
   }
}