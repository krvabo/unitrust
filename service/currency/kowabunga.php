<?php
namespace service\currency;


/**
 * Kowabunga supports soap, but also http GET and POST requests
 * For initial version we'll be using http GET.
 * 
 */
class Kowabunga implements \service\currency\currencyInterface
{
   
   /**
    * Says which currencies this converter accepts
    * 
    * @return type
    */
   public function getSupportedCurrencies()
   {
      return $this->getCurrencies();
   }
   
   
   /**
    * Connect to the website and return float result
    * 
    * @param \model\Currency $currencyFrom
    * @param \model\Currency $currencyTo
    * @param float $valueFrom
    * @return float
    * @throws \service\currency\Exception
    */
   public function convert(\model\Currency $currencyFrom, \model\Currency $currencyTo, $valueFrom)
   {
      $queryString = $this->getQueryString($currencyFrom, $currencyTo, $valueFrom);
      
      // get the conversion result or die trying (figuratively)
      try {
         $result = $this->fetch($queryString);
      } catch(\Exception $e) {
         throw $e;
      }
      
      return $this->getValueFromResult($result->getBody());
   }
   
   /**
    * get currencies from webservice and return them
    * 
    * @return array list of three letter currencies
    */
   public function getCurrencies()
   {
      try {
         $response = $this->updateCurrenciesFromSource()->getBody();
         return $this->getCurrenciesFromResponse($response);
         
      } catch (\Exception $e) {
         // if something goes wrong return an empty list
         return [];
      }
   }
   
   /**
    * Connect to converter and get a conversion response
    * 
    * @param string $queryString
    * @return \GuzzleHttp\Psr7\Response
    * @throws Exception
    */
   protected function fetch($queryString)
   {
      return $this->requestToSource('http://currencyconverter.kowabunga.net/' . $queryString);
   }
   
   
   /**
    * Try to get the value from the xml
    * 
    * @param string $resultXml
    * @return float converted value
    * @throws Exception
    */
   protected function getValueFromResult($resultXml)
   {
      $xml = simplexml_load_string($resultXml);

      if (empty($xml[0])) {
         throw new \Exception('Response of kowabunga invalid');
      }
      
      return (float)$xml[0];
   }
   
   
   /**
    * Build http query string for the kowabunga request
    * 
    * @param \model\Currency $currencyFrom
    * @param \model\Currency $currencyTo
    * @param float $valueFrom
    * @return string
    */
   protected function getQueryString(\model\Currency $currencyFrom, \model\Currency $currencyTo, $valueFrom)
   {
      return 'converter.asmx/GetConversionAmount?CurrencyFrom='
              . $currencyFrom->getShortName()
              . '&CurrencyTo=' . $currencyTo->getShortName()
              .'&RateDate=' . date('Y-m-d')
              .'&Amount=' . (float)$valueFrom;
   }
   
   /**
    * Connect to converter and get a response with currencies
    * 
    * @param string $queryString
    * @return \GuzzleHttp\Psr7\Response
    * @throws Exception
    */
   protected function updateCurrenciesFromSource()
   {
      return $this->requestToSource('http://currencyconverter.kowabunga.net/converter.asmx/GetCurrencies');
   }
   
   /**
    * Parse currencies from reponse of update command
    * 
    * @param string $resultXml
    * @throws \Exception
    * 
    * @return array of three letter codes
    */
   protected function getCurrenciesFromResponse($resultXml)
   {
      
      $xml = simplexml_load_string($resultXml);
      
      $list = (Array)$xml->string;

      if (empty($xml->string) || empty($list)) {
         throw new \Exception('Response of kowabunga invalid');
      }
      
      return $list;
      
   }
   
   /**
    * Connect to converter and get a response
    * 
    * @param string $queryString
    * @return \GuzzleHttp\Psr7\Response
    * @throws Exception
    */
   protected function requestToSource($url)
   {
      $guzzle = new \GuzzleHttp\Client();
      $result = $guzzle->request('GET', $url);
      
      if ($result->getStatusCode() != 200) {
         throw new \Exception('Failed to get proper response from kowabunga', 400);
      }
      
      return $result;
   }
}