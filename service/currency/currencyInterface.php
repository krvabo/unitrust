<?php
namespace service\currency;

interface currencyInterface
{
   
   public function convert(\model\Currency $currencyFrom, \model\Currency $currencyTo, $valueFrom);
   
   public function getSupportedCurrencies();
}